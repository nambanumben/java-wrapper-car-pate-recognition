package com.sumelongenterprise.vehicleplatenumberprocessor;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlateNumberProcessorTest {

    @Test
    public void testPlateNumberRecognition() {
        PlateNumberProcessor plateNumberProcessor = new PlateNumberProcessorGateway();
        String plateNumber = plateNumberProcessor.getPlateNumber(plateNumberProcessor.getResourceAsFile("picture.jpg"));
        assertNotNull(plateNumber);
        assertEquals("MBCL600", plateNumber.trim());
    }
}